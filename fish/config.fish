if status is-interactive
    # Commands to run in interactive sessions can go here
end
starship init fish | source

set PATH $PATH /home/martin/.local/bin
set PATH $PATH /home/martin/.dotfiles/tmux/tmuxifier/bin
set PATH $PATH /home/.config/emacs/bin
set PATH $PATH /home/martin/Applications
set -gx EDITOR emacs
set -gx BROWSER /usr/bin/librewolf

eval (tmuxifier init - fish)
