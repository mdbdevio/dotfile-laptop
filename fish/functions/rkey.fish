function rkey --wraps='gpg-connect-agent "scd serialno" "learn --force" /bye' --description 'alias rkey=gpg-connect-agent "scd serialno" "learn --force" /bye'
  gpg-connect-agent "scd serialno" "learn --force" /bye $argv; 
end
