function brn --wraps='cd ~/Emacs/Brain' --description 'alias brn=cd ~/Emacs/Brain'
  cd ~/Emacs/Brain $argv; 
end
