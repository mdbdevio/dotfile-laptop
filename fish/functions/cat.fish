function cat --wraps=bat --wraps=batcat --wraps=bactcat --description 'alias cat=batcat'
  batcat $argv; 
end
