function lss --wraps='exa -T -L=1 -B -h -l --icons' --description 'alias lss=exa -T -L=1 -B -h -l --icons'
  exa -T -L=1 -B -h -l --icons $argv; 
end
