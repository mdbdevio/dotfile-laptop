function doom --wraps='~/.emacs.d/bin/doom' --wraps=.config/emacs/bin/doom --wraps=.config/emacs/bin/./doom --description 'alias doom=.config/emacs/bin/./doom'
  .config/emacs/bin/./doom $argv; 
end
