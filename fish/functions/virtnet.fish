function virtnet --wraps='sudo virsh net-start default' --description 'alias virtnet=sudo virsh net-start default'
  sudo virsh net-start default $argv; 
end
