function mounte --wraps='sudo mount -t drvfs -o uid=1000,gid=1000 E: /mnt/e' --description 'alias mounte=sudo mount -t drvfs -o uid=1000,gid=1000 E: /mnt/e'
  sudo mount -t drvfs -o uid=1000,gid=1000 E: /mnt/e $argv; 
end
