function mountd --wraps='=sudo mount -t drvfs -o uid=1000,gid=1000 D: /mnt/d' --wraps='=mount -t drvfs -o uid=1000,gid=1000 D: /mnt/d' --wraps='mount -t drvfs -o uid=1000,gid=1000 D: /mnt/d' --wraps='sudo mount -t drvfs -o uid=1000,gid=1000 D: /mnt/d' --wraps='sudo mount -d drvfs -o uid=1000,gid=1000 F: /mnt/f' --description 'alias mountd=sudo mount -t drvfs -o uid=1000,gid=1000 D: /mnt/d'
  sudo mount -t drvfs -o uid=1000,gid=1000 D: /mnt/d $argv; 
end
