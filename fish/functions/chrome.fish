function chrome --wraps='google-chrome-stable 2>/dev/null' --description 'alias chrome=google-chrome-stable 2>/dev/null'
  google-chrome-stable 2>/dev/null $argv; 
end
