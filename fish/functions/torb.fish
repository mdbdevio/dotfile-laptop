function torb --wraps='~/Applications/tor-browser/start-tor-browser.desktop' --description 'alias torb=~/Applications/tor-browser/start-tor-browser.desktop'
  ~/Applications/tor-browser/start-tor-browser.desktop $argv; 
end
