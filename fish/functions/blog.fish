function blog --wraps='~/Emacs/blog' --wraps='~/Emacs/Blog' --wraps='cd ~/Emacs/Blog' --description 'alias blog=cd ~/Emacs/Blog'
  cd ~/Emacs/Blog $argv; 
end
