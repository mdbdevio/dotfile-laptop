#+TITLE: MDB Dev Starship Config
#+AUTHOR: MDB DEV
#+DESCRIPTION: MDB's Personal Starship Config
#+PROPERTY: header-args :tangle ~/.dotfiles/starship/starship.toml
#+auto_tangle: t
#+STARTUP: showeverything

* Table of Contents :TOC:
- [[#starship-settings-meta][Starship Settings (Meta):]]
  - [[#new-line][New Line:]]
  - [[#potential-prompt-options][Potential Prompt Options:]]
  - [[#package-module][Package Module:]]
  - [[#cmd-duration][CMD Duration:]]
  - [[#error-character][Error Character:]]
- [[#computer-stats][Computer Stats:]]
  - [[#memory-usage][Memory Usage:]]
  - [[#time][Time:]]
  - [[#username][Username:]]
  - [[#display-hostname][Display Hostname:]]
  - [[#directory][Directory:]]
  - [[#shell][Shell:]]
- [[#display-git-status][Display Git Status:]]
- [[#applications][Applications:]]
  - [[#vagrant][Vagrant:]]
  - [[#docker][Docker:]]
  - [[#kubernetes][Kubernetes:]]
- [[#languages][Languages:]]
  - [[#python][Python:]]
  - [[#rust][Rust:]]
  - [[#php][PHP:]]

* Starship Settings (Meta):
** New Line:
#+begin_src toml

# Don't print a new line at the start of the prompt
add_newline = true
#+end_src

** Potential Prompt Options:
#+begin_src toml
# Possible options for what to display in the prompt:
#prompt_order = [ "username", "hostname", "kubernetes", "directory", "git_branch", "git_commit", "git_state", "git_status", "hg_branch", "docker_context", "package", "dotnet", "elixir", "elm", "erlang", "golang", "java", "julia", "nim", "nodejs", "ocaml", "php", "purescript", "python", "ruby", "rust", "terraform", "zig", "nix_shell", "conda", "memory_usage", "aws", "env_var", "crystal", "cmd_duration", "custom", "line_break", "jobs", "battery", "time", "character", ]
#+end_src

** Package Module:
#+begin_src toml
# Disable the package module, hiding it from the prompt completely
[package]
disabled = false
#+end_src

** CMD Duration:
#+begin_src toml
[cmd_duration]
min_time = 10_000  # Show command duration over 10,000 milliseconds (=10 sec)
format = " took [$duration]($style)"
#+end_src

** Error Character:
#+begin_src toml
[character]
# this is the last character you see before the stuff you type
error_symbol = "[✖](bold red) "
#+end_src

* Computer Stats:

** Memory Usage:
#+begin_src toml
[memory_usage]
disabled = false
threshold = -1
symbol = " "
style = "bold dimmed green"
format = "$symbol[${ram}( | ${swap})]($style) "
#+end_src

** Time:
#+begin_src toml
[time]
time_format = "%T"
format = "🕙 $time($style) "
style = "bright-white"
disabled = false
#+end_src

** Username:
#+begin_src toml
[username]
style_user = "bold dimmed blue"
show_always = true
#+end_src


** Display Hostname:
#+begin_src toml
# hostname
[hostname]
ssh_only = false
format = "<[$hostname]($style)>"
trim_at = "-"
style = "bold dimmed white"
disabled = true
#+end_src

** Directory:
#+begin_src toml
[directory]
truncation_length = 5
format = "[$path]($style)[$lock_symbol]($lock_style) "
#+end_src


** Shell:

*** Display Shell:
#+begin_src toml
# env variable
[env_var.SHELL]
variable = "SHELL"
default = "unknown shell"
#+end_src

*** Shell Visual Indicator:
#+begin_src toml
[shell]
fish_indicator = ""
bash_indicator = "BSH"
powershell_indicator = "_"
unknown_indicator = "mystery shell"
disabled = false
#+end_src


* Display Git Status:
#+begin_src toml
[git_commit]
commit_hash_length = 8
style = "bold white"

[git_state]
format = '[\($state( $progress_current of $progress_total)\)]($style) '

[git_branch]
format = " [$symbol$branch]($style) "
symbol = "🍣 "
style = "bold yellow"

[git_status]
conflicted = "⚔️ "
ahead = "🏎️ 💨 ×${count}"
behind = "🐢 ×${count}"
diverged = "🔱 🏎️ 💨 ×${ahead_count} 🐢 ×${behind_count}"
untracked = "🛤️  ×${count}"
stashed = "📦 "
modified = "📝 ×${count}"
staged = "🗃️  ×${count}"
renamed = "📛 ×${count}"
deleted = "🗑️  ×${count}"
style = "bright-white"
format = "$all_status$ahead_behind"
#+end_src

* Applications:

** Vagrant:
#+begin_src toml
[vagrant]
format = "via [⍱ $version](bold white) "
#+end_src

** Docker:
#+begin_src toml
# docker
[docker_context]
format = "via [🐋 $context](blue bold)"
#+end_src

** Kubernetes:
#+begin_src toml
# kubernetes
[kubernetes]
format = 'on [⛵ $context \($namespace\)](dimmed green) '
disabled = false
[kubernetes.context_aliases]
"dev.local.cluster.k8s" = "dev"
".*/openshift-cluster/.*" = "openshift"
"gke_.*_(?P<cluster>[\\w-]+)" = "gke-$cluster"
#+end_src

* Languages:

** Python:
#+begin_src toml
[python]
format = "[$symbol$version]($style) "
style = "bold green"
#+end_src

** Rust:
#+begin_src toml
[rust]
format = "[$symbol$version]($style) "
style = "bold green"
#+end_src

** PHP:
#+begin_src toml
[php]
format = "via [🔹 $version](147 bold) "
#+end_src
